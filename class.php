<?php
class values{
    protected $title="Title";
    protected $body="Body";
    function __construct($title,$body){
        if($title != ""){
            $this->title = $title;
        }
        if($body != ""){
            $this->body = $body;
        }
    }
} 

class colorMessage extends values {
    protected $color = 'black';
    public function __set($property,$value){
        if($property == 'color'){
            $colors = array('turquoise','blue','gray');
            if(in_array($value, $colors)){
                $this->color = $value;
            }
            else{
                $this->body = "This color is invalid";
            }
        }
    }
}

class fontSize extends colorMessage{
    protected $fontSize;
    public function __set($property,$value){
        parent::__set($property,$value);
            if($property == 'fontSize'){
                if(in_array($value, range(10,24))){
                    $this->fontSize = $value.'px';
                } 
                elseif($this->body == "This color is invalid"){
                        $this->body = "color and font size are invalid";
                } 
                else{
                    $this->body = 'This font size is invalid';
                } 
            }
    }         



    public function view(){
        return "<!DOCTYPE html>
        <html>
        <head>
            <title> $this->title </title>
        </head>
        <body><p style = 'color:$this->color;font-size:$this->fontSize'>$this->body</p> </body>
        </html>";
    }
}

?>